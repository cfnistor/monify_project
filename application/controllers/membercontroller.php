<?php


class MemberController extends Controller
{

    public function index()
    {
        
    }
    
    public function viewall() {
        $currentPage = 3;
        $recordsPerPage = 224;
        $this->set('title','Monify Project');
        $member = new Member();
        $results = $member->viewAll($recordsPerPage, $currentPage);

        if (count($results) > 0) {
            $this->set('members', $results);
            $this->set('pageLink', $this->_getPageLink($currentPage, $recordsPerPage));
        }
    }

    /**
     * load members from xml file
     */
    public function load()
    {
        if (isset($_POST['submit'])) {
            $targetDir = 'xml/';
            $targetFile = $targetDir . basename($_FILES["fileToUpload"]["name"]);
            $fileType = pathinfo($targetFile, PATHINFO_EXTENSION);
        }

        if (!file_exists(XML_FILE_PATH)) {
            echo 'XML file is missing or members had been uploaded already';
        } else {
            $xml = simplexml_load_file(XML_FILE_PATH);
        }

        if ($countMembers = $xml->count()) {
            $helper = new Helper();

            //move xml into archive
            $helper->archiveXmlFile();
            
            $member = new Member();
            $member->importFromXml($xml);
            $this->set('countMembers', $countMembers);
        }
    }

    /**
     * Pagination is not done...
     *
     * @param $currentPage
     * @param $recordsPerPage
     * @return string
     */
    private function _getPageLink($currentPage, $recordsPerPage)
    {
        //TODO add pagination
        return 'Current page: ' . $currentPage . ' Results per page: ' . $recordsPerPage;
    }

    public function delete($id = null)
    {
        if (empty($id) || !is_numeric($id)) {
            echo 'Wrong parameter'; exit;
        }

        $id = (int)htmlspecialchars($id);

        $member = new Member();
        $result = $member->delete($id);

        if ($result === 1) {
            $this->set('message', 'Member account has been deleted');
        } else {
            echo 'Something went wrong';
        }

    }

}