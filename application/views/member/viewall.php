<?php $countRows = 1; ?>
<div id="pagination">
    <?php echo $pageLink;?>
</div>
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>dob</th>
            <th>address</th>
            <th>City</th>
            <th>Province</th>
            <th>Postal code</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php if (count($members) > 0):?>
        <?php foreach ($members as $member):?>
            <tr>
                <td><?php echo $countRows; ?></td>
                <td><?php echo $member['id'];?></td>
                <td><?php echo $member['first_name'];?></td>
                <td><?php echo $member['last_name'];?></td>
                <td><?php echo $member['email'];?></td>
                <td><?php echo $member['telephone'];?></td>
                <td><?php echo $member['dob'];?></td>
                <td><?php echo $member['address'];?></td>
                <td><?php echo $member['city'];?></td>
                <td><?php echo $member['province'];?></td>
                <td><?php echo $member['postal_code'];?></td>
                <td><a class="edit" href="/member/edit/<?php echo $member['id'];?>">Edit</a></td>
                <td><a class="delete" href="/member/delete/<?php echo $member['id'];?>" onclick="return confirm('Are you sure you want to delete this member?');">Delete</a></td>
            </tr>
                <?php $countRows++;?>
        <?php endforeach;?>
        <?php else:?>
            No members.
        <?php endif;?>
    </tbody>
</table>