<?php


class Member extends Model
{
    public $_resultsPerPage = 25;
    private $_columnsMapping = array(
        'first_name' => 'firstname',
        'last_name' => 'lastname',
        'email' => 'email',
        'telephone' => 'telephone',
        'dob' => 'dob',
        'address' => 'address',
        'city' => 'city',
        'province' => 'province',
        'postal_code' => "postal-code"
    );

    private $_membersCreate = "
    CREATE TABLE `members` (
      `id` int(10) unsigned NOT NULL auto_increment,
      `first_name` varchar(64) NOT NULL,
      `last_name` varchar(64) NOT NULL,
      `email` varchar(255) NOT NULL UNIQUE,
      `telephone` varchar(255) NOT NULL,
      `dob` varchar(32) NOT NULL,
      `address` varchar(255) NOT NULL,
      `city` varchar(32) NOT NULL,
      `province` varchar(64) NOT NULL,
      `postal_code` varchar(32) NOT NULL,
      `is_removed` tinyint(1) unsigned NOT NULL DEFAULT 0, 
      PRIMARY KEY  (`id`)
    );";

    private $_firstName;
    private $_lastName;
    private $_email;
    private $_telephone;
    private $_dob;
    private $_address;
    private $_city;
    private $_province;
    private $_postal_code;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->_firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->_firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->_lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->_lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->_telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->_telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getDob()
    {
        return $this->_dob;
    }

    /**
     * @param mixed $dob
     */
    public function setDob($dob)
    {
        $this->_dob = $dob;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->_address = $address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->_city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->_city = $city;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->_province;
    }

    /**
     * @param mixed $province
     */
    public function setProvince($province)
    {
        $this->_province = $province;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->_postal_code;
    }

    /**
     * @param mixed $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->_postal_code = $postal_code;
    }

    public function importFromXml($xml)
    {
        $this->_createMembersTable();
        $stmt = $this->_db->prepare("INSERT INTO $this->_table(`first_name`, `last_name`,`email`,`telephone`, `dob`, `address`,`city`, `province`, `postal_code`) VALUES(:first_name,:last_name,:email,:telephone,:dob,:address,:city,:province,:postal_code)");
        foreach ($xml as $member) {
            foreach ($this->_columnsMapping as $column => $xmlField) {
                $stmt -> bindParam(':' . $column, $member->{$xmlField});
            }
            $stmt -> execute();
        }
    }

    private function _createMembersTable()
    {
        $stmt = $this->_db->prepare($this->_membersCreate);
        $stmt -> execute();
    }
}