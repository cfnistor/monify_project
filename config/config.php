<?php

/** Configuration Variables **/

define('DEVELOPMENT_ENVIRONMENT', true);

define('DB_NAME', 'monify');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
define('DB_CHARSET','utf8');

define('BASE_URL', 'http://local.monify.com/');
define('XML_FILE_PATH', ROOT . DS . 'members.xml');
