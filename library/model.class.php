<?php

class Model {

    public $_db;
    protected $_model;

    function __construct() {
        $dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHARSET;
        $this->_db = new PDO($dsn, DB_USER, DB_PASSWORD);
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->_model = get_class($this);
        $this->_table = strtolower($this->_model)."s";
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $stmt = $this->_db->prepare('DELETE FROM ' .  $this->_table . " WHERE id=:id");
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        $affectedRows = $stmt->rowCount();

        return $affectedRows;
    }

    public function viewAll($recordsPerPage = null, $startFrom = null)
    {
        if ($recordsPerPage) {
            $offset = $recordsPerPage;
        } else {
            $offset = $this->_resultsPerPage;
        }

        if ($startFrom) {
            $start = ($startFrom - 1) *  $offset;
        } else {
            $start = 0;
        }

        $query = 'SELECT * FROM ' . $this->_table . ' LIMIT ' . $start .',' . $offset;

        $stmt = $this->_db->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
}