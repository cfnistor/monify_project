<?php

/**
 * Class Helper
 */
class Helper
{
    const MENU_ITEMS = array(
        'add' => 'Add new members',
        'edit' => 'Edit members',
        'delete' => 'Delete members',
        'viewall' => 'View all members',
        'view' => 'View single member',
        'viewdeleted' => 'View deleted members',
        'sortbyprovinces' => 'Sort members by provinces',
        'displaylast' => 'Display the last 100 members of each province based on their ID'

    );
    
    /**
     *
     */
    public function archiveXmlFile()
    {
        $archiveXmlPath = ROOT . DS . 'xml' . DS . 'members.xml';
        rename(XML_FILE_PATH, $archiveXmlPath);
    }

    public static function getMenu()
    {
        return 'Hello';
    }

}